import { validate } from './email-validator.js';

const divEl = document.querySelector('#new-section');
const sectionEl = document.createElement('section');

sectionEl.className = 'join-us';

export const SectionCreator = (type = 'standart') => {
	let title = 'Join Our Program';
	let button = 'Subscribe';
	if (type === 'advanced') {
		title = 'Join Our Advanced Program';
		button = 'Subscribe to Advanced Program ';
	}
	return (sectionEl.innerHTML = `
    <h2>${title}</h2>
    <p class="subheading">Sed do eiusmod tempor incididunt
    ut labore et dolore magna aliqua.</p>
    <form>
      <input type="email" id="email" autocomplete="off" placeholder="Email"/>
      <button class="btn" type="submit">${button} </button>
    </form>
  `);
};

export const loadSection = () => {
	divEl.append(sectionEl);
	const form = document.querySelector('form');
	const email = document.querySelector('#email');
	form.addEventListener('submit', (e) => {
		e.preventDefault();
		alert(validate(email.value));
	});
};
