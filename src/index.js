// style
import './styles/style.css';
// code
import { loadSection, SectionCreator } from './join-us-section.js';

SectionCreator('advanced');

window.addEventListener('load', loadSection);
