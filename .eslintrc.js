module.exports = {
	env: {
		browser: true,
		es2021: true,
	},
	extends: 'eslint:recommended',
	overrides: [],
	parser: '@babel/eslint-parser',
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
		requireConfigFile: false,
	},
	rules: {
		indent: ['error', 'tab'],
		'linebreak-style': ['error', 'unix'],
		quotes: ['error', 'single'],
		semi: ['error', 'always'],
		'no-undef': 'off',
		'no-console': 'error',
		'func-style': 'off',
		'func-names': 'error',
		'max-len': [1, 80, 2, { ignoreComments: true }],
		'no-unused-vars': 0,
	},
};
